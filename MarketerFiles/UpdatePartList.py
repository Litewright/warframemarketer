import json
import logging
import logging.handlers
import os

import requests
from bs4 import BeautifulSoup


class PartList:
    def __init__(self):
        # File references
        self.item_reference = os.path.join("MemoryFiles", "ItemList.json")
        self.item_location_reference = os.path.join("MemoryFiles", "ItemLocations.json")
        self.log_filename = os.path.join("Logs", "logging_rotating_file.out")

        # Collective list of all websites where we can get information from
        self.item_location_list = self.retrieve_json(self.item_location_reference)

        # Setup the logging to evaluate when things go wrong
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        self.handler = logging.handlers.RotatingFileHandler(self.log_filename, encoding='utf8', maxBytes=100000,
                                                            backupCount=1)
        self.formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler(self.handler)

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: retrieve_json
    # Arguments:    file_reference = a reference to the file being retrieved
    # Returns: A dict or list depending on the format of the associated json file
    # Purpose: Return the contents of a json file

    def retrieve_json(self, file_reference):
        try:
            # Return the json file to the user, either as dict or a list.
            return json.load(open(file_reference, "r"))
        # Handle the associated errors with the program
        except KeyError as err:
            self.logger.info("Error - %s" % err)
            self.logger.info("Update item from json file %s" % self)
        except IOError as err:
            self.logger.info("Error - %s" % err)
            self.logger.info("Update item from json file %s" % self)
        except AttributeError as err:
            self.logger.info("Error - %s" % err)
            self.logger.info("Update item from json file %s" % self)

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: update_json_list
    # Arguments:    file_reference = a reference to the file being retrieved
    #               update_list = A string or list that contains information to be added to json file that is formatted
    #                             in a list
    # Returns: The updated list with all information needed
    # Purpose: To update the json file with the associated reference and return the completed list generated

    def update_json_list(self, file_reference, update_list):
        # Get the old information from the list so that we can add new information onto it
        old_data = self.retrieve_json(file_reference)
        # Depending on the type that update_list is, either append the string or concatenate the list
        if update_list is str:
            old_data.append(update_list)
        elif update_list is list:
            old_data += update_list
        # Attempt to dump the updated information back into the file and return it to the person who called the function
        try:
            json.dump(old_data, open(file_reference, "w"))
            return old_data
        except KeyError as err:
            self.logger.info("Error - %s" % err)
            self.logger.info("Update item from json file %s" % self)
        except IOError as err:
            self.logger.info("Error - %s" % err)
            self.logger.info("Update item from json file %s" % self)
        except AttributeError as err:
            self.logger.info("Error - %s" % err)
            self.logger.info("Update item from json file %s" % self)

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: update_json_dict
    # Arguments:    file_reference = a reference to the file being retrieved
    #               update_list = A string or list that contains information to be added to json file that is formatted
    #                             in a list
    #               update_key = The key for the specific section of the dict you wish to update
    # Returns: The updated dict with all the updated information within the dict
    # Purpose: To update the json file with the associated reference and return the completed dict generated

    def update_json_dict(self, file_reference, update_list, update_key):
        # Get the old information from the list so that we can add new information onto it
        old_data = self.retrieve_json(file_reference)
        # Get the list breakdown of the key you want to update
        old_list = old_data[update_key]
        # Depending on the type that update_list is, either replace the string or concatenate the list
        if update_list is str:
            old_list = update_list
        elif update_list is list:
            old_list += update_list
        # Attempt to dump the updated information back into the file and return it to the person who called the function
        try:
            json.dump(old_list, open(file_reference, "w"))
            return old_list
        except KeyError as err:
            self.logger.info("Error - %s" % err)
            self.logger.info("Update item from json file %s" % self)
        except IOError as err:
            self.logger.info("Error - %s" % err)
            self.logger.info("Update item from json file %s" % self)
        except AttributeError as err:
            self.logger.info("Error - %s" % err)
            self.logger.info("Update item from json file %s" % self)

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: add_parts
    # Purpose: To update the current list of warframe parts by checking for new parts that can be sold

    def add_parts(self):
        # Set up r to handle the request that will be made
        r = requests
        # Attempt to make the request, if the request fails handle it as an except and print it to the log
        try:
            r = requests.get(self.item_location_list[0])
        except requests.exceptions.RequestException as err:
            self.logger.info("Error - %s" % err)
            self.logger.info("Connection Error on Requests module - Check Connection - %s" % self)
            return None
        # Parse the request as a text with the beautiful soup parser
        soup = BeautifulSoup(r.text, 'html.parser')
        # Find the parts based off the class they are and the name they have
        part_list = soup.find_all('p', {'class': "MsoNormal"})
        # Provide a filter to filter out elements from the part_list
        filter_tuple = tuple(["OROKIN", "TOWER", "ROTATION"])
        # Convert the result set given in part_list into a list of the string contents for processing.
        text_list = [result.text.strip() for result in part_list]
        # Filter out anything beginning with Orokin, Tower or Rotation. That just details where we can get the items
        matching_string = [x for x in text_list if not x.startswith(filter_tuple)]
        # Filter out empty elements in the list
        complete_list = [x for x in matching_string if x]
        # Remove duplicate items from the list
        de_duplicated_list = list(set(complete_list))
        # Sort the list
        de_duplicated_list.sort()
        # Update the new contents of the part list onto the file containing all other items
        self.update_json_list(self.item_reference, de_duplicated_list)
